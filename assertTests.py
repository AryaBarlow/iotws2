import unittest
import morse



def testEncode(root):
    eExpected = ['.', '.', '-', ' ', '.','.','.']
    e = morse.encode(root, 'us', 0)
    assert e == eExpected, "Should be ..- ..."
    
    print("Encoding the phrase 'us'\nReturned ", end="")

    for i in e:
        print(i,end="")

    print("\nExpected ",end="")
    for i in eExpected:
        print(i,end="")

    print("\nYour encoder works perfectly!")

def testDecode(root):
    eExpected = ['U','S']
    e = morse.decode(root, list("..- ..."),3)
    assert e == eExpected, "Should be ..- ..."

    print("Decoding '..- ...'\nReturned ", end="")

    for i in e:
        print(i,end="")

    print("\nExpected ",end="")
    for i in eExpected:
        print(i,end="")

    print("\nYour decoder works perfectly!")

class TestEncodeUni(unittest.TestCase):
    def test_encode_uni(self):
        root = morse.makeSomeMorse()
        eExpected = ['.', '.', '-', ' ', '.','.','.']
        e = morse.encode(root, 'us', 0)

        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")

    def test_decode_uni(self):
        root = morse.makeSomeMorse()
        eExpected = ['U','S']
        e = morse.decode(root, list("..- ..."),3)
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")
        
        

if __name__ == '__main__':
    
    unittest.main()

