Worksheet 2 Part 1
------------------

    All of the specified tasks for worksheet 2 part 1 have been completed

    To see that the node tree is created and printed properly, you may simply run the morse.py file as the main.
    Alternatively, by importing morse into another python file and first running root = morse.makeSomeMorse(), you can run the morse functions from anywhere.

    morse.makeSomeMorse() requires no parametres to create the morse-code binary tree and return its root node.
    Most every morse function requires you to pass in this root node, so don't forget it.

    To see a UnitTest of morse.encode and morse.decode between "us" and "..- ...", run the file assertTests.py
    To see the functions themselves return debugging data for this test, run the file main.py

    To witness all other tests, run the file morseunit.py

    The first five tests are of the encoder and demonstrate morse.encode()'s capability to translate even phrases consisting of several, space separated words. The first four tests will always succeed, but the fifth test is intended to fail by asserting equality to an incorrect translation.

    MORSE ENCODE
    ------------

        Morse.Encode requires three Parametres
            1) The Root node of the binary tree
            2) A list containing the English phrase to be encoded into morse
            3) an integer value of 0 determining whether an encoded phrase should be returned or simply the node containing a letter.
            This third paramater is utilized by morse.find() in order to return nodes by the character they contain, without re-writing large blocks of code.

    MORSE DECODE
    ------------

        The next five tests are of the decoder and demonstrate morse.decode()'s capacity to translate morse code into multiple space seperated words by way of typing one extra space between the morse letters and symbols to be translated like so ".... --- .-- -.. -.--  .--. .- .-. - -. . .-." 
        Note the two spaces                                                                                        ^^ here to show that one white space should be placed between the first word "Howdy" and the second word "Partner" of the translation
        The first four tests will always succeed, but the fifth one will fail because it deliberately asserts equality to an incorrect translation.

        Morse.Decode requires three Paramatres
            1) The Root node of the binary tree
            2) A list containing the morse code to be translated into english
            3) an integer that determines what is returned by the function.
                
                Setting this integer to "1" will return a list containing the path taken (Left and Right) through the tree to find the character and the morse code of the character, followed by the character itself, followed by a "\n."
                This is used by morse.printTree() function to ensure that three was created successfully. It could however be used to find the speciffic addresses of several letters in a list, simulataniously.

                Setting this integer to "2" will return the english translation of exactly one character translated from morse code. This is used by morse.encode() to check lots of morse codes quickly against the english to be trnalsated.

                Setting this integer to "3" is usefull for translating entire words or even sentences from morse into english by using multiple loops and adding each correctly translated letter to the returned value.

                Setting this integer to "4" returns only the node containing the letter matching the specified morse code. This is used by the morse.find() function to find a node by using its morse code.

    TREE IS EMPTY AND TREE IS FULL
    ------------------------------

        Test Tree Is Empty and Test Tree Is FUll both utilize morse.py's checkTree() function
        Test Tree Is Empty will only succeed if the entire tree is empty
        Test Tree Is Full will only succeed if the entire tree is full
        Both of these tests will fail, as the tree is neither empty nor entire full, meaning that the function morse.checkTree() is working correctly

        morse.checkTree requires three parametres
            1) the root node of the binary tree
            2) how many layers of tree should be searched (to prevent errors from searching non-existent nodes in layers beyond the tree's scope)
            3) A string value of either "NotFull" or "NotEmpty" which return an integer of how many nodes are missing and how many nodes are full respectively.

    FIND BY MORSE AND FIND BY ENGLISH
    ---------------------------------
        Test Find By Morse and Test Find By English both utilize morse.py's find() function which uses the following paramaters;
            1) The root node of the tree,
            2) a single character ("M") for finding a node by morse and ("E") for finding by english,
            3) a list containing the morse/english to be found in the tree

        Providing that a matching node exists, the find function will return the node assigned to the morse code or english character matching your input.
        The tests are performed by ensuring that the data of the returned node is not equal to "", confirming that a matching node was in fact returned.
        The test will succeed

    INSERTION AND DELETION
    ----------------------
        Test Insertion tests morse.insert()'s capability to insert new characters to the tree. The function requires the following paramatres;
            1) the root node,
            2) a list of morse characters, serving as directions for where in the node tree our new data should be inserted,
            3) a string containing the inserted data

        The test is performed by confirming that the inserted data can be found in the tree by using the function morse.find()
        This test will succeed

        Test Deletion tests morse.delete() which is almost identical to morse.insert(), save for only requiring two paramatres
            1) the root node
            2) a list of morse characters, serving as directions for where in the node tree our new data should be inserted
            
        These are used to find the desired node and set all data inside of it to "VOID"
        The test is performed by deleting all data in the node containing the letter ("M")
        morse.find is used to pull data from that same node.
        If that data is now equal to "VOID" then the test is succeeded
        This test will succeed

    EVEN MORE CHARACTERS!
    ---------------------
        The final two tests ensure that the new characters added to our binary tree at the end of the worksheet are also functional with morse.encode() and morse.decode(). These tests are performed like previous encode and decode tests, only using the newly added characters.
        These tests will succeed.

Worksheet 2 Part 2
------------------

    Decode_bt
    ---------
    Decoding the binary heap is only slightly different from decoding the binary tree. Both iterate through multiple symbols and words represented by morse in the same way - isolating each symmbol and then running a smaller loop to translate it and add the result to the returning variable.  

    Even when translating individual symbols from their corresponding morse code, in both cases the string of morse is used as a series of directions to be looped through one character at a time. With the binary tree, we begin with a pointer to the root node. Each "." or "-" in the string moves our pointer onwards to the left or right child respectively until the string is exhausted and the data of the node currently being pointed to is returned. With the binary heap, this node pointer is instead replaced with an integer starting at 0, representing a position in the array of all character. As per the formulae provided, each "." multiplies this integer by two and adds one to the result, and each "-" does likewise except for adding 2 to the result. Once the string is exhausted, the integer is returned and should point to the translated character in the array

    Again an implementation using dictionaries would work identically to with the binary heap and tree where iterating through the morse phrase to find spaces and isolate individual symbols for conversion is concerned. The sole difference is that no smaller loop or embedded function would be required to translate the isolated symbol into english. Instead, only a single line of code would be required "dict.get("MORSE HERE")". The rest of the implementation would again work as normal. Defining the dictionary itself is uniquely simple. There would be no need to track your place in a binary tree or consider the alogrythm used for traversing the contents of a binary heap. Instead, the dictionary is defined "matter-of-factly" as "dict = { "key": "translation", }
    
    Encode_Ham
    ----------
    The encode_ham function can be seen working in the HeapUni.py tests. It is capable of encoding a sender, recipient and message consisting of multiple words into a single string of morse code. It's parametres are as follows.

    1) The morse heap
    2) The sender name
    3) The recipient name
    4) The message to be encoded

    Decode_Ham
    ----------

    The encode_ham function can be seen working in the HeapUni.py tests. It is capable of decoding and separating a sender, recient and message from a single string of morse in the following format.

    Reciever using individual spaces between morse characters. Multiple words cannot be used. 
    [A double space to separate]
    Sender using individual spaces between morse characters. Multiple words cannot be used. 
    [A souble space to separate]
    A message using single spaces to seperate individual characters and double spaces to separate mutliple words. 

    This would allow for...
    Recipient "John"
    Sender "Arthur"
    Message "Hey, man. It's been a while."

    The parametres are as follows.
    1) The morse heap
    2) the string of morse to be decoded.

    Send Echo
    -----------------------
    Send echo utilizes 2 paramaetres
    1) sender: str,
    2) msg: str

    It operates by encoding these into morse_ham that is addressed to the server's Echo, sending it and returning the server's responce, which is always an identical message, save for the sender and recipient being reversed. You can see this function be tested by running the mains of morseServer.py or HeapUni.py

    Send Time
    -----------------------
    Send echo utilizes 1 paramaetres
    1) sender: str,

    It operates by encoding the sender into morse_ham that is addressed to the server's Time, sending it and returning the server's responce, which is the current time (minus one hour) addressed to the user. You can see this function be tested by running the mains of morseServer.py or HeapUni.py