
class Node:

    def __init__(self, data):

        self.left = None
        self.right = None
        self.data = data




def makeSomeMorse():
    root = Node("VOID")

    #Layer 1
    root.left = Node("E")
    root.right = Node("T")

    #Layer 2
    root.left.left = Node("I")
    root.left.right = Node("A")
    root.right.left = Node("N")
    root.right.right = Node("M")

    #Layer 3
    root.left.left.left = Node("S")
    root.left.left.right = Node("U")
    root.left.right.left = Node("R")
    root.left.right.right = Node("W")

    root.right.left.left = Node("D")
    root.right.left.right = Node("K")
    root.right.right.left = Node("G")
    root.right.right.right = Node("O")

    #Layer 4
    root.left.left.left.left = Node("H")
    root.left.left.left.right = Node("V")
    root.left.left.right.left = Node("F")
    root.left.left.right.right = Node("VOID")

    root.left.right.left.left = Node("L")
    root.left.right.left.right = Node("VOID")
    root.left.right.right.left = Node("P")
    root.left.right.right.right = Node("J")

    root.right.left.left.left = Node("B")
    root.right.left.left.right = Node("X")
    root.right.left.right.left = Node("C")
    root.right.left.right.right = Node("Y")

    root.right.right.left.left = Node("Z")
    root.right.right.left.right = Node("Q")
    root.right.right.right.left = Node("VOID")
    root.right.right.right.right = Node("VOID")

    #LAYER 5
    root.left.left.left.left.left = Node("5")
    root.left.left.left.left.right = Node("4")
    root.left.left.left.right.left = Node("VOID")
    root.left.left.left.right.right = Node("3")

    root.left.left.right.left.left = Node("VOID")
    root.left.left.right.left.right = Node("¿")
    root.left.left.right.right.left = Node("?")
    root.left.left.right.right.right = Node("2")

    root.left.right.left.left.left = Node("&")
    root.left.right.left.left.right = Node("VOID")
    root.left.right.left.right.left = Node("+")
    root.left.right.left.right.right = Node("VOID")

    root.left.right.right.left.left = Node("VOID")
    root.left.right.right.left.right = Node("VOID")
    root.left.right.right.right.left = Node("VOID")
    root.left.right.right.right.right = Node("1")

    root.right.left.left.left.left = Node("6")
    root.right.left.left.left.right = Node("=")
    root.right.left.left.right.left = Node("/")
    root.right.left.left.right.right = Node("VOID")

    root.right.left.right.left.left = Node("VOID")
    root.right.left.right.left.right = Node("VOID")
    root.right.left.right.right.left = Node("(")
    root.right.left.right.right.right = Node("VOID")

    root.right.right.left.left.left = Node("7")
    root.right.right.left.left.right = Node("VOID")
    root.right.right.left.right.left = Node("VOID")
    root.right.right.left.right.right = Node("VOID")

    root.right.right.right.left.left = Node("8")
    root.right.right.right.left.right = Node("VOID")
    root.right.right.right.right.left = Node("9")
    root.right.right.right.right.right = Node("0")

    #Layer 6
    root.left.left.left.left.left.left = Node("VOID")
    root.left.left.left.left.left.right = Node("VOID")
    root.left.left.left.left.right.left = Node("VOID")
    root.left.left.left.left.right.right = Node("VOID")

    root.left.left.left.right.left.left = Node("VOID")
    root.left.left.left.right.left.right = Node("VOID")
    root.left.left.left.right.right.left = Node("VOID")
    root.left.left.left.right.right.right = Node("VOID")

    root.left.left.right.left.left.left = Node("VOID")
    root.left.left.right.left.left.right = Node("VOID")
    root.left.left.right.left.right.left = Node("VOID")
    root.left.left.right.left.right.right = Node("VOID")

    root.left.left.right.right.left.left = Node(")")
    root.left.left.right.right.left.right = Node("-")
    root.left.left.right.right.right.left = Node("VOID")
    root.left.left.right.right.right.right = Node("VOID")

    root.left.right.left.left.left.left = Node("VOID")
    root.left.right.left.left.left.right = Node("VOID")
    root.left.right.left.left.right.left = Node('"')
    root.left.right.left.left.right.right = Node("VOID")

    root.left.right.left.right.left.left = Node("VOID")
    root.left.right.left.right.left.right = Node(".")
    root.left.right.left.right.right.left = Node("VOID")
    root.left.right.left.right.right.right = Node("VOID")

    root.left.right.right.left.left.left = Node("VOID")
    root.left.right.right.left.left.right = Node("VOID")
    root.left.right.right.left.right.left = Node("VOID")
    root.left.right.right.left.right.right = Node("VOID")

    root.left.right.right.right.left.left = Node("VOID")
    root.left.right.right.right.left.right = Node("VOID")
    root.left.right.right.right.right.left = Node("'")
    root.left.right.right.right.right.right = Node("VOID")

    root.right.left.left.left.left.left = Node("VOID")
    root.right.left.left.left.left.right = Node("-")
    root.right.left.left.left.right.left = Node("VOID")
    root.right.left.left.left.right.right = Node("VOID")

    root.right.left.left.right.left.left = Node("VOID")
    root.right.left.left.right.left.right = Node("VOID")
    root.right.left.left.right.right.left = Node("VOID")
    root.right.left.left.right.right.right = Node("VOID")

    root.right.left.right.left.left.left = Node("VOID")
    root.right.left.right.left.left.right = Node("VOID")
    root.right.left.right.left.right.left = Node(";")
    root.right.left.right.left.right.right = Node("!")

    root.right.left.right.right.left.left = Node("VOID")
    root.right.left.right.right.left.right = Node("VOID")
    root.right.left.right.right.right.left = Node("VOID")
    root.right.left.right.right.right.right = Node("VOID")

    root.right.right.left.left.left.left = Node("VOID")
    root.right.right.left.left.left.right = Node("¡")
    root.right.right.left.left.right.left = Node("VOID")
    root.right.right.left.left.right.right = Node("VOID")

    root.right.right.left.right.left.left = Node("VOID")
    root.right.right.left.right.left.right = Node("VOID")
    root.right.right.left.right.right.left = Node("VOID")
    root.right.right.left.right.right.right = Node("VOID")

    root.right.right.right.left.left.left = Node(":")
    root.right.right.right.left.left.right = Node("VOID")
    root.right.right.right.left.right.left = Node("VOID")
    root.right.right.right.left.right.right = Node("VOID")

    root.right.right.right.right.left.left = Node("VOID")
    root.right.right.right.right.left.right = Node("VOID")
    root.right.right.right.right.right.left = Node("VOID")
    root.right.right.right.right.right.right = Node("VOID")

    #Layer 7
    root.left.left.left.right.left.left.right = Node("$")

    return(root)


