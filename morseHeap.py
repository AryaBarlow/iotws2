import morse

def makeHeap(root,layers):
    curLayer = 1
    morseHeap = list([root.data])

    #A search string is constucted, beginning with a singular "." or "LEFT"
    searchString = list(".")

    while(curLayer <= layers):

        #Starting from the root, the search string is used as a set of instructions to find and then print the desired node
        morseHeap.append(morse.decode(root, searchString, 2))

        #The search string is then incremented as though it were a binary number. When the entire string consists of "-" a new layer is made accesible
        #By adding one character to the string and then setting all characters to "."
        searchString = morse.increment(searchString, (len(searchString) - 1))

        #The current Layer is therefore equal to the length of the searchString, which is used to gauge whether we have searched to the specified limit
        curLayer = len(searchString)


    return(morseHeap)

def decode_bt(morseHeap, searchString):
    retMe = list("")
    #Used to iterate through the entire search string
    count = 0

    #This loop handles the entire string, including spaces
    while(count < len(searchString)):

        miniString = list("")
        #While this loop only handles individual words, turning them into a miniString
        while(count < len(searchString) and searchString[count] != " "):
            miniString += searchString[count]
            count += 1
			
		#The Ministring is processed by findByMorse() and the single translated character is added to the returned value
        retMe += morseHeap[findByMorse(miniString)]
			
		#When translating multiple words at once - these statements responds to there being two blank spaces next to eachother
        #This is treated as the start of a new word, and so they are skipped over and a single space is printed.
        if ((count + 1) < len(searchString)):
            if (searchString[count] == " " and searchString[count + 1] == " "):
                count += 1
                retMe += " "

        count += 1
    
    #if (retMe[-1] == ''):
    #    del retMe[-1]

    return(retMe)

#This function can take the morse code for a single character and return its position in the morse heap
def findByMorse(searchString):
    pos = 0
    for i in searchString:
        if (i == "."):
            pos = (pos * 2) + 1
        if (i == "-"):
            pos = ((pos * 2) + 2)
    return(pos)


def encode_ham(root, sender, reciever, msg):
    recTemp = (reciever + "de")
    reciever2 = morse.encode(root, recTemp, 0)

    sender2   = morse.encode(root, sender, 0)

    msgTemp = ("=" + msg + "=(")
    msg2      = morse.encode(root, msgTemp, 0)

    tup       = (reciever2, sender2, msg2)
    fin       = tup[0] + list("  ") + tup[1] + list("  ") + tup[2]
    return(fin)

def decode_ham(morseHeap, msg):
    sender = list("")
    reciever = list("")
    searchString = list("")
    spaceCount = 0

    limit = len(msg)
    x = 0
    while x < limit:
        
        if spaceCount == 0:
            reciever += msg[x]
        elif spaceCount == 1:
            sender += msg[x]
        elif spaceCount == 2:
            searchString += msg[x]

        if msg[x] == " ":
            if msg[x + 1] == " ":
                if (spaceCount < 2):
                    spaceCount += 1
                    x += 1
        x += 1
        
    retReciever = decode_bt(morseHeap,reciever)
    retSender = decode_bt(morseHeap,sender)
    message = decode_bt(morseHeap,searchString)
        
    tup = (retReciever, retSender, message)
    return(tup)


#Creates a Morse Code Tree
if __name__ == "__main__":
    print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    root = morse.makeSomeMorse()
    #morse.printTree(root,6)

    morseHeap = makeHeap(root, 6)
    for i in morseHeap:
        print(i,end=" ")

    print("\n\n")

    phrase = decode_bt(morseHeap, "-. . ...- . .-.  -.-. .- .-.. .-..  -- .  ... .... . .-. .-.. . -.--")
    for i in phrase:
        print(i,end="")
    print("\nExpected: NEVER CALL ME SHERLEY")

    thisDict = {
    ".": "A",
    "-": "B",
    "..": "C",
    ".-": "D"
    }
    print(thisDict.get("-"))


    print("\n")
    nuPhrase = ".- .-.. -.. . ... - -...- .--. --- - -...- -.--."
    phrase = decode_bt(morseHeap, nuPhrase)
    for i in phrase:
        print(i,end="")
    print("\n")

    
    #print("Encode st, al, pot")
    #hold = encode_ham(root, "St", "Al", "Pot")
    #for i in hold:
    #    print(i,end="")
    #print("")

    print("Encode Paul, John, Hello You")
    hold = encode_ham(root, "Paul", "John", "Hello You")
    for i in hold:
        print(i,end="")
    print("")


    print("Decode result")
    #print(decode_bt(morseHeap, hold[0]),end="")
    #print(decode_bt(morseHeap, hold[1]),end="")
    #print(decode_bt(morseHeap, hold[2]),end="")
    #print(decode_bt(morseHeap, hold),end="")
    #print("\n\n")

    nuTup = decode_ham(morseHeap, hold)
    print(nuTup[0])
    print(nuTup[1])
    print(nuTup[2])
