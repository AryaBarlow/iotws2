import unittest
import morse

class TestEncodeUni(unittest.TestCase):

    ##########################################################################
    def test_encode_01(self):
        root = morse.makeSomeMorse()
        eExpected = list("..- ...")
        e = morse.encode(root, 'us', 0)
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")

    def test_encode_02(self):
        root = morse.makeSomeMorse()
        eExpected = list(".... --- .-- -.. -.--  .--. .- .-. - -. . .-.")
        e = morse.encode(root, 'Howdy Partner', 0)
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")

    def test_encode_03(self):
        root = morse.makeSomeMorse()
        eExpected = list("-... . .-- .- .-. .  - .... .  ...- .- -- .--. .. .-. .")
        e = morse.encode(root, 'Beware the Vampire', 0)
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")

    def test_encode_04(self):
        root = morse.makeSomeMorse()
        eExpected = list("-. . ...- . .-.  -.-. .- .-.. .-..  -- .  ... .... . .-. .-.. . -.--")
        e = morse.encode(root, 'Never Call Me Sherley', 0)
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")

    def test_encode_05(self):
        root = morse.makeSomeMorse()
        eExpected = list("................................")
        e = morse.encode(root, 'Never Call Me Sherley', 0)
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")
        
    ##########################################################################

    def test_decode_01(self):
        root = morse.makeSomeMorse()
        eExpected = list("US")
        e = morse.decode(root, list("..- ..."),3)
        self.assertEqual(e, eExpected)
        print("Your decoder works perfectly!")

    def test_decode_02(self):
        root = morse.makeSomeMorse()
        eExpected = list("HOWDY PARTNER")
        e = morse.decode(root, list(".... --- .-- -.. -.--  .--. .- .-. - -. . .-."),3)
        self.assertEqual(e, eExpected)
        print("Your decoder works perfectly!")

    def test_decode_03(self):
        root = morse.makeSomeMorse()
        eExpected = list("BEWARE THE VAMPIRE")
        e = morse.decode(root, list("-... . .-- .- .-. .  - .... .  ...- .- -- .--. .. .-. ."),3)
        self.assertEqual(e, eExpected)
        print("Your decoder works perfectly!")

    def test_decode_04(self):
        root = morse.makeSomeMorse()
        eExpected = list("NEVER CALL ME SHERLEY")
        e = morse.decode(root, list("-. . ...- . .-.  -.-. .- .-.. .-..  -- .  ... .... . .-. .-.. . -.--"),3)
        self.assertEqual(e, eExpected)
        print("Your decoder works perfectly!")

    def test_decode_05(self):
        root = morse.makeSomeMorse()
        eExpected = list("Never Call Me Sherley")
        e = morse.decode(root, list("................................"),3)
        self.assertEqual(e, eExpected)
        print("Your decoder works perfectly!")

##################################################################################################################

    def testTreeIsEmpty(self):
        root = morse.makeSomeMorse()
        res = morse.checkTree(root,5,"NotEmpty")
        if res == 0:
            print("The tree is empty")
        else:
            print("The tree is not empty")
        self.assertEqual(res, 0)

    def testTreeIsFull(self):
        root = morse.makeSomeMorse()
        res = morse.checkTree(root,5,"NotFull")
        if res == 0:
            print("The tree is full")
        else:
            print("The tree is not full")
        self.assertEqual(res, 0)

    def testFindByMorse(self):
        root = morse.makeSomeMorse()

        res = morse.find(root, "M", list("..---")).data
        if (res != ""):
            print("..--- found successfully")
        
        self.assertEqual(res, "2")

    def testFindByEnglish(self):
        root = morse.makeSomeMorse()

        res = morse.find(root, "E", list("2")).data
        if (res != ""):
            print("2 found successfully")
        
        self.assertEqual(res, "2")

    def testInsertion(self):
        root = morse.makeSomeMorse()

        morse.insert(root, list("----"), "/")

        res = morse.find(root, "E", list("/")).data
        if (res != "/"):
            print("/ found successfully")
        
        self.assertEqual(res, "/")

    def testDeletion(self):
        root = morse.makeSomeMorse()

        morse.delete(root, list("----"))

        res = morse.find(root, "M", list("----")).data
        if (res == "VOID"):
            print("VOID found successfully")
        
        self.assertEqual(res, "VOID")

    def test_encode_new_character(self):
        root = morse.makeSomeMorse()
        eExpected = list("---...")
        e = morse.encode(root, ':', 0)
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")

    def test_decode_new_character(self):
        root = morse.makeSomeMorse()
        eExpected = list(":")
        e = morse.decode(root, list("---..."),3)
        self.assertEqual(e, eExpected)
        print("Your decoder works perfectly!")


if __name__ == '__main__':
    unittest.main()
