import morse
import unittest
import assertTests

if __name__ == "__main__":

    root = morse.makeSomeMorse()
    assertTests.testEncode(root)
    print()
    assertTests.testDecode(root)