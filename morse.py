import createTree

#Adds an additional character to the search string, making it possibe to venture to a further layer of the binary tree
def moveLayer(searchString):
    searchString += "."
    count = 0
    
    #All characters are again set to . allowing us to once again iterate from the left-most path to the right-most
    while (count < len(searchString)):
        searchString[count] = "."
        count += 1
    return(searchString)

#Recurcively Increments a search String like a binary number, starting with the furthest character to the right which is initially held by targ
def increment(searchString, targ):

    #In this case, think of "." as 0 and "-" as 1. We are trying to increment a binary number by 1
    
    if (searchString[targ] == "."):
        searchString[targ] = '-'

    elif (searchString[targ] == "-"):
        searchString[targ] = '.'
        
        targ-=1
        #If the right-most number is already 1, it cannot become 2. Instead, it is set to 0 and we instead attempt to increment the number to its left
        if (targ >= 0):
            searchString = increment(searchString, targ)

        #If, however, there are no numbers to the left then we need to create a new layer by adding another position to the binary number
        #All values are set back to 0, ready to be iterated again until they once more meet their limit
        #This structure is necesary for the tree to print in order of layers, rather than "trajerctory" or sorts, which can be confusing
        else:
            searchString = moveLayer(searchString)

    return(searchString)
    
def insert(root, searchString, value):
    count = 0
    curNode = root

    #USes the search string to find the desired insert point
    while(count < len(searchString) and searchString[count] != " "):
        if (searchString[count] == "."):
            curNode = curNode.left
        if (searchString[count] == "-"):
            curNode = curNode.right
        count += 1

    curNode.data = value

def delete(root, searchString):
    insert(root,searchString, "VOID")
    
def decode(root, searchString, compare):
    
    retMe = list("")
    #Used to iterate through the entire search string
    count = 0

    #This loop handles the entire string, including spaces
    while(count < len(searchString)):
        curNode = root

        #While this loop only handles individual words
        while(count < len(searchString) and searchString[count] != " "):
            if (searchString[count] == "."):
                curNode = curNode.left
            if (searchString[count] == "-"):
                curNode = curNode.right
            count += 1
        
        #Compare one will print the tree, morse and english translation
        if (compare == 1):

            #In Tree Form
            for i in searchString:
                if (i == "."):
                    retMe += "L"
                else:
                    retMe += "R"
            retMe += " "

            #In Morse Form
            for i in searchString:
                retMe += i
            retMe += " "

            #In English
            retMe += " = " + curNode.data + "\n"


        #Used for encoding
        elif (compare == 2):
            return(curNode.data)

        #Otherwise, only the english translation is printed without spaces between individual letters
        elif (compare == 3):
            retMe += curNode.data

        elif(compare == 4):
            return(curNode)

        #When translating multiple words at once - these statements responds to there being two blank spaces next to eachother
        #This is treated as the start of a new word, and so they are skipped over and a single space is printed.
        if ((count + 1) < len(searchString)):
            if (searchString[count] == " " and searchString[count + 1] == " "):
                count += 1
                retMe += " "

        count += 1
    
    if (retMe[-1] == ''):
        del retMe[-1]

    return(retMe)
    
def checkTree(root, layers, search):
    curLayer = 1

    #A search string is constucted, beginning with a singular "." or "LEFT"
    searchString = list(".")
    checkFail = 0

    while(curLayer <= layers):

        #Starting from the root, the search string is used as a set of instructions to find and then print the desired node
        hold = decode(root, searchString, 3)

        if (search == "NotFull"):
            if hold == list("VOID"):
                checkFail += 1

        if (search == "NotEmpty"):
            if hold != list("VOID"):
                checkFail += 1
            

        #The search string is then incremented as though it were a binary number. When the entire string consists of "-" a new layer is made accesible
        #By adding one character to the string and then setting all characters to "."
        searchString = increment(searchString, (len(searchString) - 1))

        #The current Layer is therefore equal to the length of the searchString, which is used to gauge whether we have searched to the specified limit
        curLayer = len(searchString)
    
    return checkFail

#Counting the empty node as layer 0, this function prints a tree upto a number of layers specified by the user, starting from layer 1
def printTree(root, layers):
    curLayer = 1

    #A search string is constucted, beginning with a singular "." or "LEFT"
    searchString = list(".")

    while(curLayer <= layers):

        #Starting from the root, the search string is used as a set of instructions to find and then print the desired node
        hold = decode(root, searchString, 1)
        for i in hold:
            print(i,end="")

        #The search string is then incremented as though it were a binary number. When the entire string consists of "-" a new layer is made accesible
        #By adding one character to the string and then setting all characters to "."
        searchString = increment(searchString, (len(searchString) - 1))

        #The current Layer is therefore equal to the length of the searchString, which is used to gauge whether we have searched to the specified limit
        curLayer = len(searchString)


#Translates an English phrase into Morse Code
def encode(root, phrase, retNode):
    
    translation = list("")

    #Iterates through the entire phrase
    pit = 0
    while (pit < len(phrase)):
        
        #If the character is a space then the translation is simply 1 space
        if (phrase[pit] == ' '):
            translation += " "

        #Otherwise, the character is searched for in the node tree, using the same SearchString technique employed by decoding
        else:
            curLayer = 1
            searchString = list(".")
            while(curLayer <= 6):
                
                #If the letter matches the node, then the search string is added to the translation + 1 space
                if (decode(root, searchString, 2) == (phrase[pit].upper())):

                    #Used for the find function
                    if (retNode == 1):
                        return(decode(root, searchString, 4))

                    for i in searchString:
                        translation += i
                    translation += " "
                    curLayer = 7

                #Otherwise the searchString is incremented until it matches
                else:
                    searchString = increment(searchString, (len(searchString) - 1))
                    curLayer = len(searchString)
                
        pit += 1
    
    del translation[-1]
    return(translation)

def makeSomeMorse():
    return(createTree.makeSomeMorse())

def find(root, findBy, search):
    if (findBy.upper() == "M" or findBy.upper() == "MORSE"):
        retMe = decode(root, search, 4)
    else:
        retMe = encode(root, search, 1)
    return(retMe)

#Creates a Morse Code Tree
if __name__ == "__main__":
    root = makeSomeMorse()
    

    printTree(root,6)
    print("\n\n")

    #print(checkTree(root,5,"NotEmpty"))
    #print(checkTree(root,5,"NotFull"))

    #print("\n\n")
    #for i in ((decode(root, list("..---  .-.-.  ..---  -...-  ....-  -... .- -... -.--"), 3))):
    #    print(i,end="")

    #print("\n\n")
    #engPhrase = encode(root, "Never call me Sherley", 0)

    #for i in engPhrase:
    #    print(i,end="")

    #Finding by using morse and english
    #print("Right here")
    #print(find(root, "M",list("..---")).data)
    #print("Right here")
    #print(find(root, "E",list("2")).data)

    
    #for i in (decode(root, engPhrase, 3)):
    #    print(i,end="")

