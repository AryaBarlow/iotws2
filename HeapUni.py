import morseHeap
import morse
import unittest
import morseServer
import asyncio


class TestEncodeUni(unittest.TestCase):
    ##########################################################################

    def test_decode_01(self):
        root = morse.makeSomeMorse()
        mh = morseHeap.makeHeap(root,6)

        eExpected = list("US")
        e = morseHeap.decode_bt(mh, list("..- ..."))
        self.assertEqual(e, eExpected)
        print("Your decoder works perfectly!")

    def test_decode_02(self):
        root = morse.makeSomeMorse()
        mh = morseHeap.makeHeap(root,6)

        eExpected = list("NEVER CALL ME SHERLEY")
        e = morseHeap.decode_bt(mh, list("-. . ...- . .-.  -.-. .- .-.. .-..  -- .  ... .... . .-. .-.. . -.--"))
        self.assertEqual(e, eExpected)
        print("Your decoder works perfectly!")

    def test_extended_morse_encoder_01(self):
        root = morse.makeSomeMorse()
        

        eExpected = list(".- .-.. -.. .  ... -  -...- .--. --- - -...- -.--.")
        e = morseHeap.encode_ham(root, "St", "Al", "Pot")
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")

    def test_extended_morse_encoder_02(self):
        root = morse.makeSomeMorse()
        

        eExpected = list(".--- --- .... -. -.. .  .--. .- ..- .-..  -...- .... . .-.. .-.. ---  -.-- --- ..- -...- -.--.")
        e = morseHeap.encode_ham(root, "Paul", "John", "Hello You")
        self.assertEqual(e, eExpected)
        print("Your encoder works perfectly!")

    def test_extended_morse_decoder_01(self):
        root = morse.makeSomeMorse()
        mh = morseHeap.makeHeap(root,6)

        hold = morseHeap.encode_ham(root, "St", "Al", "Pot")
        e = morseHeap.decode_ham(mh, hold)

        eExpected0 = list("ALDE")
        eExpected1 = list("ST")
        eExpected2 = list("=POT=(")
        self.assertEqual(e[0], eExpected0)
        self.assertEqual(e[1], eExpected1)
        self.assertEqual(e[2], eExpected2)
        print("Your decoder works perfectly")

    def test_extended_morse_decoder_02(self):
        root = morse.makeSomeMorse()
        mh = morseHeap.makeHeap(root,6)

        hold = morseHeap.encode_ham(root, "Paul", "John", "Hello You")
        e = morseHeap.decode_ham(mh, hold)

        eExpected0 = list("JOHNDE")
        eExpected1 = list("PAUL")
        eExpected2 = list("=HELLO YOU=(")
        self.assertEqual(e[0], eExpected0)
        self.assertEqual(e[1], eExpected1)
        self.assertEqual(e[2], eExpected2)
        print("Your decoder works perfectly")

    def test_send_echo(self):
        e = asyncio.run(morseServer.send_echo("arya","potato"))
        expected = ".- .-. -.-- .- -.. . . -.-. .... --- -...- .--. --- - .- - --- -...- -.--."
        self.assertEqual(e, expected)
        print("Your echo works perfectly")

    def test_send_time(self):
        e = asyncio.run(morseServer.send_time("Jeff"))
        expected = ""
        self.assertNotEqual(e, expected)
        print("Your send time works perfectly")

if __name__ == '__main__':
    unittest.main()