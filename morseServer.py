#!/usr/bin/env python
#source ../iot_env/bin/activate

import asyncio
import json
import websockets
import morse
import morseHeap

async def talk(websocket, message, client_id):
    outward_message = {
        'client_id': client_id,
        'type': 'morse_evt',
        'payload': message
    }
    await websocket.send(json.dumps(outward_message))

async def recv_message(websocket):
    message = json.loads(await websocket.recv())
    return message['payload']

#For testing purposes. Provides translates during communication with server.
async def mServe():
    uri = "ws://localhost:10102"
    async with websockets.connect(uri) as websocket:
        message = json.loads(await websocket.recv())
        print(message)

        # Get the client_id from the join message
        if message['type'] == 'join_evt':
            client_id = message['client_id']
        else:
            # If first message is not the join message exit
            print("Did not receive a correct join message")
            return 0

        m = morse.makeSomeMorse()
        mh = morseHeap.makeHeap(m, 6)

        hold = morseHeap.encode_ham(m, "DAVE", "ECHO", "Hello")
        str1 = ''.join(hold)
        print("Morse = " + str1)

        await talk(websocket, str1, client_id)
        response = await recv_message(websocket)
        print("Serve = " + response)
        print("")

        print("My decode is")
        nuTup = morseHeap.decode_ham(mh, hold)
        str2 = ''.join(nuTup[0])
        str3 = ''.join(nuTup[1])
        str4 = ''.join(nuTup[2])
        print(str2 + str3 + str4)
        print("")

        print("Their decode is is")
        nuTup = morseHeap.decode_ham(mh, response)
        str2 = ''.join(nuTup[0])
        str3 = ''.join(nuTup[1])
        str4 = ''.join(nuTup[2])
        print(str2 + str3 + str4)
        print("")

        hold = morseHeap.encode_ham(m, "DAVE", "TIME", "12:00")
        str1 = ''.join(hold)
        print("Morse = " + str1)

        await talk(websocket, str1, client_id)
        response = await recv_message(websocket)
        print("Serve = " + response)
        print("")

        print("Their decode is is")
        nuTup = morseHeap.decode_ham(mh, response)
        str2 = ''.join(nuTup[0])
        str3 = ''.join(nuTup[1])
        str4 = ''.join(nuTup[2])
        print(str2 + str3 + str4)
        print("")

async def send_echo(sender: str, msg: str):
    uri = "ws://localhost:10102"
    async with websockets.connect(uri) as websocket:
        message = json.loads(await websocket.recv())
        print(message)

        # Get the client_id from the join message
        if message['type'] == 'join_evt':
            client_id = message['client_id']
        else:
            # If first message is not the join message exit
            print("Did not receive a correct join message")
            return 0

        #Encode's the user's message for sending to the server
        m = morse.makeSomeMorse()
        hold = morseHeap.encode_ham(m, sender, "ECHO", msg)
        str1 = ''.join(hold)

        #Sends encoded message to server and returns the responce
        await talk(websocket, str1, client_id)
        response = await recv_message(websocket)
        return(response)



async def send_time(sender: str):
    uri = "ws://localhost:10102"
    async with websockets.connect(uri) as websocket:
        message = json.loads(await websocket.recv())
        print(message)

        # Get the client_id from the join message
        if message['type'] == 'join_evt':
            client_id = message['client_id']
        else:
            # If first message is not the join message exit
            print("Did not receive a correct join message")
            return 0

        #Encodes the user message to be sent to the server
        m = morse.makeSomeMorse()
        hold = morseHeap.encode_ham(m, sender, "TIME", "")
        str1 = ''.join(hold)

        #Sends the user message to server and returns its responce.
        await talk(websocket, str1, client_id)
        response = await recv_message(websocket)
        return(response)

#send_echo(sender: str, msg: str) -> str
#send_time(sender: str) -> str

if __name__ == "__main__":
    m = morse.makeSomeMorse()
    mh = morseHeap.makeHeap(m, 6)

    #asyncio.run(mServe())
    hold = asyncio.run(send_echo("arya","potato"))

    print("\n\n\n")
    for i in hold:
        print(i,end="")
    print("\n\n\n")

    nuTup = morseHeap.decode_ham(mh, hold)
    str2 = ''.join(nuTup[0])
    str3 = ''.join(nuTup[1])
    str4 = ''.join(nuTup[2])
    print("Their decoded message reads " + str2 + str3 + str4)
    print("")

    hold = asyncio.run(send_time("Kevin"))
    nuTup = morseHeap.decode_ham(mh, hold)
    str2 = ''.join(nuTup[0])
    str3 = ''.join(nuTup[1])
    str4 = ''.join(nuTup[2])
    print("Their decoded message reads " + str2 + str3 + str4)
    print("")
